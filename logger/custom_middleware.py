import asyncio
import logging
from django.core.cache import cache
import datetime
import ipaddress
import redis

logger = logging.getLogger(__name__)


class SimpleMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response
        # One-time configuration and initialization.

        self.blacklist = set()
        self.max_visits = 5
        self.ip_watcher = redis.Redis(db=1)

    async def blacklist_routine(self):
        _, ip = self.ip_watcher.blpop("ips")
        if ip:
            print(ip)
            ip = ipaddress.ip_address(ip.decode("utf-8"))
            now = datetime.datetime.utcnow()

            ip_timestamp = f"{ip}:{now.minute}"
            n = self.ip_watcher.incrby(ip_timestamp, 1)
            print(n)
            if n >= self.max_visits:
                print(f"Hat bot detected!:  {ip}")
                self.blacklist.add(ip)

            else:
                print(f"{now}:  saw {ip}")

            _ = self.ip_watcher.expire(ip_timestamp, 60)

    def __call__(self, request):
        """Code to be executed for each request before
           the view (and later middleware) are called."""
        # asyncio.create_task(self.blacklist_routine())
        # print(self.blacklist)

        response = self.get_response(request)

        """Code to be executed for each request/response after
           the view is called. """
        x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
        if x_forwarded_for:
            ip = x_forwarded_for.split(',')[0]
        else:
            ip = request.META.get('REMOTE_ADDR')

        cache.set('ips', ip)

        logger.critical("{} - {}".format(request.user, ip))

        return response
